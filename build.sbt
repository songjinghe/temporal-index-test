name := "temporal-index-scala"

version := "0.1"

scalaVersion := "2.12.6"

resolvers += "aliyun" at "http://maven.aliyun.com/nexus/content/groups/public/"

libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.3.2"
libraryDependencies += "org.apache.commons" % "commons-compress" % "1.17"