package edu.buaa.act.rtree.str

class Bound(minVal:(Long, Long, Long, Int), maxVal:(Long, Long, Long, Int)){
    val min:(Long, Long, Long, Int) = minVal
    val max:(Long, Long, Long, Int) = maxVal

    def compareTo(o: Bound, dimIndex:Int): Boolean = {
        dimIndex match {
            case 0 => this.min._1 + this.max._1 < o.min._1 + o.max._1
            case 1 => this.min._2 + this.max._2 < o.min._2 + o.max._2
            case 2 => this.min._3 + this.max._3 < o.min._3 + o.max._3
            case 3 => this.min._4 + this.max._4 < o.min._4 + o.max._4
        }
    }
}
