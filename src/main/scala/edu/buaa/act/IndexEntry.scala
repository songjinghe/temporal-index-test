package edu.buaa.act

class IndexEntry(eid:Long, s:Long, e:Long, v:Int) {
    var entityId:Long = eid
    var start:Long = s
    var end:Long = e
    var value:Int = v
}
