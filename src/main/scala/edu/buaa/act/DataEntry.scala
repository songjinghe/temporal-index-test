package edu.buaa.act

class DataEntry(eid:Long, time:Long, travel:Int, jam:Byte, carN:Byte, segN:Int) {
    var entityId:Long = eid
    var start:Long = time
    var travelTime:Int = travel
    var jamStatus:Byte = jam
    var carCount:Byte = carN
    var segCount:Int = segN
}
